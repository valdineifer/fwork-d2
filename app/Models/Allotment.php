<?php
namespace App\Models;

use Core\BaseModel;
use Core\Connection;
use PDO;

class Allotment extends BaseModel {
	private $id, $number, $condom_id;

	function __construct($attributes = NULL){
		if($attributes) {
			$this->id = empty($attributes['id']) ? null : $attributes['id'];
			$this->number = empty($attributes['number']) ? null : $attributes['number'];
			$this->condom_id = empty($attributes['condom_id']) ? null : $attributes['condom_id'];
		}
	}

	function getId() { return $this->id; }

	function getNumber() { return $this->number; }
	function setNumber($number) { $this->number = $number; }

    function getCondom_id() { return $this->condom_id; }
    function setCondom_id($condom_id) { $this->condom_id = $condom_id; }

	public function readWithCondom() {
		$connection = Connection::connect();
		$query = "SELECT `allotment`.`id`, `allotment`.`number`, `condom`.`name` AS `condom` FROM `allotment` JOIN `condom` ON `allotment`.`condom_id` = `condom`.`id`";
		$stmt = $connection->prepare($query);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_CLASS, get_called_class());
	}
}
