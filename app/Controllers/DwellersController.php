<?php
	namespace App\Controllers;

	use Core\BaseController;
	use Core\Container;
	use App\Models\Dweller;
	use App\Controllers\AllotmentsController;
	use Core\Email;

	class DwellersController extends BaseController {
		public function index() {
			$model = Container::getModel("Dweller");
			$this->view->dwellers = $model->readWithAllotment();

			$this->setPageTitle("Moradores");
			$this->renderView('dweller/index', 'layout');
		}

		public function create() {
			$model = Container::getModel("Allotment");
			$this->view->allotments = $model->select();

			$this->setPageTitle("Criar Morador");
			$this->renderView('dweller/create', 'layout');
		}
		public static function insert($request) {
			if(!isset($request->post->dweller['id']) && !empty($request->post->dweller['name']) && !empty($request->post->dweller['email'] && !empty($request->post->dweller['allotment_id']))){
				$dweller = new Dweller($request->post->dweller);
				try {
					$dweller->insert();
				} catch (PDOException $e) {
					echo $e->getMessage();
					exit();
				}
				header("Location: /dwellers");
			}
		}

		public function edit($id) {
			$model = Container::getModel("Allotment");
			$this->view->allotments = $model->select();

			$this->id = $id;
			$this->view->dweller = DwellersController::read($id)[0];

			$this->setPageTitle("Editar Morador");
			$this->renderView('dweller/create', 'layout');
		}
		public static function update($request){
			if(!empty($request->post->dweller['id']) && !empty($request->post->dweller['name']) && !empty($request->post->dweller['email'] && !empty($request->post->dweller['allotment_id']))){
				$dweller = new Dweller($request->post->dweller);
				try {
					$dweller->update();
				} catch (PDOException $e) {
					echo $e->getMessage();
					exit();
				}
				header("Location: /dwellers");
			}
		}

		public static function delete($id) {
			if(!empty($id)) {
				try {
					Dweller::delete(['id' => $id]);
				}
				catch (PDOException $e) {
					echo $e->getMessage();
					exit();
				}
			}
			header("Location: /dwellers");
		}

		public function email($id) {
			Email::send($id);
		}

		public static function read($id=null){
			return Dweller::select($id);
		}
	}