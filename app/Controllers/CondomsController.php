<?php
	namespace App\Controllers;

	use App\Models\Condom;
	use App\Controllers\CompaniesController;
	use Core\BaseController;
	use Core\Container;

	class CondomsController extends BaseController {
		public function index() {
			$model = Container::getModel("Condom");
			$this->view->condoms = $model->readWithCompany();

			$this->setPageTitle("Condomínios");
			return $this->renderView('condom/index', 'layout');
		}

		public function create() {
			$model = Container::getModel("Company");
			$this->view->companies = $model->select();

			$this->setPageTitle("Criar Condomínio");
			$condom = Container::getModel("Condom");
			return $this->renderView('condom/create', 'layout');
		}
		public function insert($request){
			if(!isset($request->post->condom['id']) && !empty($request->post->condom['name']) && !empty($request->post->condom['company_id'])){
				$condom = new Condom($request->post->condom);
				try {
					$condom->insert();
				} catch (PDOException $e) {
					echo $e->getMessage();
					exit();
				}
				header("Location: /condoms");
			}
		}

		public function edit($id) {
			$model = Container::getModel("Company");
			$this->view->companies = $model->select();

			$this->id = $id;
			$this->setPageTitle("Editar Condomínio");
			$this->condom = $this->read($id)[0];
			return $this->renderView('condom/edit', 'layout');
		}
		public static function update($request){
			if(!empty($request->post->condom['id']) && !empty($request->post->condom['name']) && !empty($request->post->condom['company_id'])){
				$condom = new Condom($request->post->condom);
				try {
					$condom->update();
				} catch (PDOException $e) {
					echo $e->getMessage();
					exit();
				}
				header("Location: /condoms");
			}
		}

		public static function delete($id) {
			if(!empty($id)) {
				try {
					Condom::delete(['id' => $id]);
				}
				catch (PDOException $e) {
					echo $e->getMessage();
					exit();
				}
			}
			header("Location: /condoms");
		}

		public static function read($id=null){
			return Condom::select($id);
		}
	}
