<?php
namespace App\Controllers;

use Core\BaseController;
use Core\Container;
use App\Models\Allotment;
use App\Controllers\CondomsController;

class AllotmentsController extends BaseController {
	public function index() {
		$model = Container::getModel("Allotment");
		$this->view->allotments = $model->readWithCondom();

		$this->setPageTitle("Lotes");
		$this->renderView('allotment/index', 'layout');
	}

	public function create() {
		$model = Container::getModel("Condom");
		$this->view->condoms = $model->select();

		$this->setPageTitle("Criar Lotes");
		$this->renderView('allotment/create', 'layout');
	}
	public function insert($request){
		if(!isset($request->post->allotment['id']) && !empty($request->post->allotment['number']) && !empty($request->post->allotment['condom_id'])){
			$allotment = new Allotment($request->post->allotment);
			try {
				$allotment->insert();
			} catch (PDOException $e) {
				echo $e->getMessage();
				exit();
			}
			header("Location: /allotments");
		}
	}

	public function edit($id) {
		$model = Container::getModel("Condom");
		$this->view->condoms = $model->select();

		$this->id = $id;
		$this->allotment = AllotmentsController::read($id)[0];

		$this->setPageTitle("Editar Lote");
		$this->renderView('allotment/edit', 'layout');
	}
	public static function update($request){
		if(!empty($request->post->allotment['id']) && !empty($request->post->allotment['number']) && !empty($request->post->allotment['condom_id'])){
			$allotment = new Allotment($request->post->allotment);
			try {
				$allotment->update();
			} catch (PDOException $e) {
				echo $e->getMessage();
				exit();
			}
			header("Location: /allotments");
		}
	}

	public static function delete($id) {
		if(!empty($id)) {
			try {
				Allotment::delete(['id' => $id]);
			}
			catch (PDOException $e) {
				echo $e->getMessage();
				exit();
			}
		}
		header("Location: /allotments");
	}

	public static function read($id=null){
		return Allotment::select($id);
	}
}
