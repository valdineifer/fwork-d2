<?php
namespace App\Controllers;

use App\Models\Company;
use Core\BaseController;
use Core\Container;

class CompaniesController extends BaseController {
	public function index() {
		$model = Container::getModel("Company");
		$this->view->companies = $model->select();

		$this->setPageTitle("Empresas");
		return $this->renderView('company/index', 'layout');
	}

	public function create() {
		$this->setPageTitle("Empresas");
		$company = Container::getModel("Company");
		return $this->renderView('company/create', 'layout');
	}
	public function insert($request) {
		if($request->post->company['name'] && $request->post->company['cnpj']){
			$company = new Company($request->post->company);
			try {
				$company->insert();
			} catch (PDOException $e) {
				echo $e->getMessage();
				exit();
			}
			header("Location: /companies");
		}
	}


	public function edit($id) {
		$this->setPageTitle("Editar Empresa");
		$this->id = $id;
		$this->company = CompaniesController::read($id)[0];
		return $this->renderView('company/edit', 'layout');
	}
	public static function update($request){
		var_dump($request);
		if(!empty($request->post->company['id']) && !empty($request->post->company['name']) && !empty($request->post->company['cnpj'])){
			$company = new Company($request->post->company);
			try {
				$company->update();
			} catch (PDOException $e) {
				echo $e->getMessage();
				exit();
			}
			header("Location: /companies");
		}
	}

	public static function delete($id) {
		if(!empty($id)) {
			try {
				Company::delete(['id' => $id]);
			}
			catch (PDOException $e) {
				echo $e->getMessage();
				exit();
			}
		}
		header("Location: /companies");
	}

	public static function read($id=null){
		return Company::select($id);
	}
}