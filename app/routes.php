<?php

$route[] = ['/', 'HomeController@index'];

$route[] = ['/companies', 'CompaniesController@index'];
$route[] = ['/company/create', 'CompaniesController@create'];
$route[] = ['/company/insert', 'CompaniesController@insert'];
$route[] = ['/company/{id}/edit', 'CompaniesController@edit'];
$route[] = ['/company/update', 'CompaniesController@update'];
$route[] = ['/company/{id}/delete', 'CompaniesController@delete'];

$route[] = ['/condoms', 'CondomsController@index'];
$route[] = ['/condom/create', 'CondomsController@create'];
$route[] = ['/condom/insert', 'CondomsController@insert'];
$route[] = ['/condom/{id}/edit', 'CondomsController@edit'];
$route[] = ['/condom/update', 'CondomsController@update'];
$route[] = ['/condom/{id}/delete', 'CondomsController@delete'];

$route[] = ['/allotments', 'AllotmentsController@index'];
$route[] = ['/allotment/create', 'AllotmentsController@create'];
$route[] = ['/allotment/insert', 'AllotmentsController@insert'];
$route[] = ['/allotment/{id}/edit', 'AllotmentsController@edit'];
$route[] = ['/allotment/update', 'AllotmentsController@update'];
$route[] = ['/allotment/{id}/delete', 'AllotmentsController@delete'];

$route[] = ['/dwellers', 'DwellersController@index'];
$route[] = ['/dweller/create', 'DwellersController@create'];
$route[] = ['/dweller/insert', 'DwellersController@insert'];
$route[] = ['/dweller/{id}/edit', 'DwellersController@edit'];
$route[] = ['/dweller/update', 'DwellersController@update'];
$route[] = ['/dweller/{id}/delete', 'DwellersController@delete'];
$route[] = ['/dweller/{id}/email', 'DwellersController@email'];

return $route;
