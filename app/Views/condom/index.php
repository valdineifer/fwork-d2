<h1> Todos os condomínios </h1>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nome</th>
      <th scope="col">Empresa</th>
      <th scope="col">Deletar</th>
      <th scope="col">Editar</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($this->view->condoms as $condom): ?>
    <tr>
      <th scope="row"><?=$condom->getId()?></th>
      <td><?= $condom->getName(); ?></td>
      <td><?= $condom->company; ?></td>
      <td><a href= "/condom/<?=$condom->getId()?>/delete" ><button type="button" class="btn btn-danger">Deletar</button> </a></td>
      <td><a href= "/condom/<?= $condom->getId()?>/edit" ><button type="button" class="btn btn-warning">Editar</button> </a></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
<a href= "/condom/create" ><button type="button" class="btn btn-success">Adicionar novo condomínio</button> </a>
