<form action="/condom/<?= !empty($this->id) ? 'update': 'insert' ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Nome</label>
    <input type="text" class="form-control" id="exampleInputEmail1" name="condom[name]" placeholder="Nome" value="<?= !empty($this->id) ? $this->condom->getName() : '' ?>">
  </div>
  <div class="form-group">
    <label for="companies">Empresas</label>
    <select class="form-control" id="companies" name="condom[company_id]">
      <?php foreach($this->view->companies as $company): ?>
      	<?php if(!empty($this->id) && ($company->getId() == $this->condom->getCompany_id())): ?>
      		<option selected value="<?= $company->getId() ?>"> <?php echo $company->getName() ?> </option>
      	<?php endif; ?>
      	<?php if(!isset($this->id) || ($company->getId() != $this->condom->getCompany_id())): ?>
      		<option value="<?= $company->getId() ?>"> <?php echo $company->getName() ?> </option>
      	<?php endif; ?>
      <?php endforeach; ?>
    </select>
  </div>
	<?php if (isset($this->id)): ?>
    <input type="hidden" class="form-control" name="condom[id]" value="<?= $this->id ?>" >
	<?php endif; ?>

  <button type="submit" name="action" class="btn btn-primary">Enviar</button>
</form>
