<h1> Todas as empresas </h1>
<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nome</th>
            <th scope="col">Cnpj</th>
            <th scope="col">Deletar</th>
            <th scope="col">Editar</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($this->view->companies as $company): ?>
        <tr>
            <th scope="row"><?= $company->getId(); ?></th>
            <td><?= $company->getName(); ?></td>
            <td><?= $company->getCnpj(); ?></td>
            <td><a href= "/company/<?=$company->getId()?>/delete" ><button type="button" class="btn btn-danger">Deletar</button> </a></td>
            <td><a href= "/company/<?=$company->getId() ?>/edit" ><button type="button" class="btn btn-warning">Editar</button> </a></td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<a href= "/company/create" ><button type="button" class="btn btn-success">Adicionar nova empresa</button> </a>