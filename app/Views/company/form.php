<form action="/company/<?= !empty($this->id) ? 'update' : 'insert' ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Nome</label>
    <input type="text" class="form-control" id="exampleInputEmail1" name="company[name]" aria-describedby="emailHelp" placeholder="Nome" value="<?= !empty($this->id) ? $this->company->getName() : '' ?>">
  </div>
  <div class="form-group">
    <label for="CNPJ">Cnpj</label>
    <input type="text" class="form-control" name="company[cnpj]" id="CNPJ" placeholder="CNPJ" minlength="14" maxlength="14" value="<?= !empty($this->id) ? $this->company->getCnpj() : '' ?>">
  </div>
	<?php if (isset($this->id)): ?>
    <input type="hidden" class="form-control" name="company[id]" value="<?= $this->id?>" >
	<?php endif; ?>

  <button type="submit" name="action" class="btn btn-primary">Enviar</button>
</form>