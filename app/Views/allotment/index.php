<h1> Todos os Lotes </h1>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Número</th>
      <th scope="col">Condomínio</th>
      <th scope="col">Deletar</th>
      <th scope="col">Editar</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($this->view->allotments as $allotment): ?>
    <tr>
      <th scope="row"><?=$allotment->getId()?></th>
      <td><?= $allotment->getNumber(); ?></td>
      <td><?= $allotment->condom; ?></td>
      <td><a href= "allotment/<?=$allotment->getId()?>/delete" ><button type="button" class="btn btn-danger">Deletar</button> </a></td>
      <td><a href= "allotment/<?= $allotment->getId() ?>/edit" ><button type="button" class="btn btn-warning">Editar</button> </a></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
<a href= "/allotment/create" ><button type="button" class="btn btn-success">Adicionar novo lote</button> </a>
