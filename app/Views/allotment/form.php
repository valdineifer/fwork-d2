<form action="/allotment/<?= !empty($this->id) ? 'update': 'insert' ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Número</label>
    <input type="number" class="form-control" id="exampleInputEmail1" name="allotment[number]" value="<?= !empty($this->id) ? $this->allotment->getNumber() : '' ?>">
  </div>
  <div class="form-group">
    <label for="condoms">Condomínio</label>
    <select class="form-control" id="condoms" name="allotment[condom_id]">
      <?php foreach($this->view->condoms as $condom): ?>
      	<?php if(isset($this->id) && ($condom->getId() == $this->allotment->getcondom_id())): ?>
      		<option selected value="<?= $condom->getId() ?>"> <?php echo $condom->getName() ?> </option>
      	<?php endif; ?>
      	<?php if(!isset($this->id) || ($condom->getId() != $this->allotment->getcondom_id())): ?>
      		<option value="<?= $condom->getId() ?>"> <?php echo $condom->getName() ?> </option>
      	<?php endif; ?>
      <?php endforeach; ?>
    </select>
  </div>
	<?php if (isset($this->id)): ?>
    <input type="hidden" class="form-control" name="allotment[id]" value="<?= $this->id ?>" >
	<?php endif; ?>

  <button type="submit" name="action" value="<?= !empty($this->id) ? 'update' : 'create' ?>" class="btn btn-primary">Enviar</button>
</form>
