<h1> Todos os moradores </h1>
<table class="table">
	<thead class="thead-dark">
		<tr>
			<th scope="col">#</th>
			<th scope="col">Nome</th>
			<th scope="col">Email</th>
			<th scope="col">Lote</th>
			<th scope="col">Deletar</th>
			<th scope="col">Editar</th>
			<th scope="col">Ligar</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($this->view->dwellers as $dweller): ?>
		<tr>
			<th scope="row"><?=$dweller->getId()?></th>
			<td><?= $dweller->getName(); ?></td>
			<td><?= $dweller->getEmail(); ?></td>
			<td><?= $dweller->allotment; ?></td>
			<td><a href= "/dweller/<?=$dweller->getId()?>/delete" ><button type="button" class="btn btn-danger">Deletar</button> </a></td>
			<td><a href= "/dweller/<?= $dweller->getId() ?>/edit" ><button type="button" class="btn btn-warning">Editar</button> </a></td>
			<td><a href= "/dweller/<?= $dweller->getId() ?>/email" ><button type="button" class="btn btn-info">Ligar</button> </a></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<a href= "/dweller/create" ><button type="button" class="btn btn-success">Adicionar novo morador</button> </a>
