<form action="/dweller/<?= !empty($this->id) ? 'update': 'insert' ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Nome</label>
    <input type="text" class="form-control" id="exampleInputEmail1" name="dweller[name]" placeholder="Nome" value="<?= !empty($this->id) ? $this->view->dweller->getName() : '' ?>">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail">Email</label>
    <input type="email" class="form-control" id="exampleInputEmail" placeholder="Email" name="dweller[email]" value="<?= !empty($this->id) ? $this->view->dweller->getEmail() : '' ?>">
  </div>
  <div class="form-group">
    <label for="allotments">Lotes</label>
    <select class="form-control" id="allotments" name="dweller[allotment_id]">
      <?php foreach($this->view->allotments as $allotment): ?>
      	<?php if(isset($this->id) && ($allotment->getId() == $this->view->dweller->getAllotment_id())): ?>
      		<option selected value="<?= $allotment->getId() ?>"> <?php echo $allotment->getNumber() ?> </option>
      	<?php endif; ?>
      	<?php if(!isset($this->id) || ($allotment->getId() != $this->view->dweller->getAllotment_id())): ?>
      		<option value="<?= $allotment->getId() ?>"> <?php echo $allotment->getNumber() ?> </option>
      	<?php endif; ?>
      <?php endforeach; ?>
    </select>
  </div>
	<?php if (isset($this->id)): ?>
    <input type="hidden" class="form-control" name="dweller[id]" value="<?= $this->id ?>" >
	<?php endif; ?>

  <button type="submit" name="action" class="btn btn-primary">Enviar</button>
</form>
